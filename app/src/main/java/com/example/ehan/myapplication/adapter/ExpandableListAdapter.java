package com.example.ehan.myapplication.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ehan.myapplication.R;
import com.example.ehan.myapplication.entity.DataItem;
import com.squareup.picasso.Picasso;

import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<DataItem> mainCategory; //header Title
//    private HashMap<String,List<String>> subCategory; // list child

    public ExpandableListAdapter(Context context, List<DataItem> mainCategory) {
        this.context = context;
        this.mainCategory = mainCategory;

    }

    @Override
    public int getGroupCount() {
        return this.mainCategory.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.mainCategory.get(groupPosition).getSubCate().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.mainCategory.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.mainCategory.get(groupPosition).getSubCate().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            String headerTitle = mainCategory.get(groupPosition).getCateName();
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            TextView lblListHeader = convertView.findViewById(R.id.tvListHeader);
            TextView subCateSize=convertView.findViewById(R.id.tvCateSize);
            ImageView mainCateImage=convertView.findViewById(R.id.imageView);
            lblListHeader.setText(headerTitle);
            subCateSize.setText(""+getChildrenCount(groupPosition));

        if( mainCategory.get(groupPosition).getIconName()!="" && isValid(mainCategory.get(groupPosition).getIconName())){
            Picasso.get().load(mainCategory.get(groupPosition).getIconName()).into(mainCateImage);
        }

            return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = mainCategory.get(groupPosition).getSubCate().get(childPosition).getCateName();
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this.context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }
            TextView subCateName = convertView.findViewById(R.id.tvListChild);
            subCateName.setText("  "+childText);
            return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public boolean isValid(String url){
        try{
            new URL(url).toURI();
            return true;
        }catch (Exception e){
            return false;
        }
    }
}
