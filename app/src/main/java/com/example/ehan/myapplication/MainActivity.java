package com.example.ehan.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;


import com.example.ehan.myapplication.adapter.ExpandableListAdapter;
import com.example.ehan.myapplication.data.ServiceGenerator;
import com.example.ehan.myapplication.data.remote.CategoryService;
import com.example.ehan.myapplication.entity.DataItem;
import com.example.ehan.myapplication.entity.Response;
import com.example.ehan.myapplication.entity.SubCateItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    List<DataItem> categoryList;
    ExpandableListView exListView;
    ExpandableListAdapter adapter;

    CategoryService service;
    private CompositeDisposable disaposal=new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        categoryList =new ArrayList<>();

        exListView=findViewById(R.id.exListView);

        service=ServiceGenerator.createService(CategoryService.class);

        getAllCategoryRx();
    }

    private void getAllCategoryRx(){
        disaposal.add(
                service.getCategoriesRx()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<Response>() {
                    @Override
                    public void onNext(Response response) {
                        categoryList.addAll(response.getData());
                        Log.e("LoadData", "onNext: "+ categoryList.toString());
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                        adapter=new ExpandableListAdapter(MainActivity.this,categoryList);
                        exListView.setAdapter(adapter);
                        exListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                 Toast.makeText(MainActivity.this,"Clicked :  "+
                                            categoryList.get(groupPosition).getSubCate().get(childPosition).getCateName(), Toast.LENGTH_SHORT)
                                            .show();
                                 return false;
                            }
                        });
                    }
                })
        );
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        disaposal.clear();
    }
}
