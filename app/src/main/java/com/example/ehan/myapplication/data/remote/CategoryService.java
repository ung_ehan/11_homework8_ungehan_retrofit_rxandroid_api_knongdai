package com.example.ehan.myapplication.data.remote;

import com.example.ehan.myapplication.entity.Response;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoryService {

    @GET("api/v1/categories")
    Call<Response> getCategories();

//    @GET("api/v1/categories")
//    Observable<CategoryResponse> getCategoriesRx();

    @GET("api/v1/categories")
    Observable<Response> getCategoriesRx();

}
